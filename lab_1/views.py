from django.shortcuts import render
from datetime import date

# Enter your name here
mhs_name = 'Kemala Andiyani' # TODO Implement this
year = 1998

# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age':calculate_age(year)}
    return render(request, 'index.html', response)

# TODO Implement this to complete last checklist
def calculate_age(birth_year):
    return date.today().year-birth_year
